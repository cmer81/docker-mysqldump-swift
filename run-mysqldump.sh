#!/bin/bash

set -x
DATE=`date +%F`

mkdir /mnt/backup/
/usr/bin/mysqldump --single-transaction --quick --lock-tables=false -h $MARIADB_SERVER -u $MARIADB_USER --password=$MARIADB_PASSWORD  $MARIADB_DATABASE | gzip -9 > /mnt/backup/$DATE-backup.sql.gz
swift upload $BUCKET /mnt/backup/$DATE-backup.sql.gz --object-name $DATE-backup.sql.gz
swift post $BUCKET $DATE-backup.sql.gz -H "X-Delete-After:1209600"
rm -rf /mnt/backup/$DATE-backup.sql.gz