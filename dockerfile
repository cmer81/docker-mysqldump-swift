FROM centos:7

RUN yum install mariadb python-setuptools -y \
&& easy_install pip \
&& pip install python-swiftclient python-keystoneclient
ADD run-mysqldump.sh /opt
RUN chmod +x /opt/run-mysqldump.sh

ENTRYPOINT [ "/opt/run-mysqldump.sh" ]